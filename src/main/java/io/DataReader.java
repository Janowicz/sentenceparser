package io;

import java.util.Scanner;

public class DataReader {

    private Scanner scanner = new Scanner(System.in);
    private ConsolePrinter printer;

    public DataReader(ConsolePrinter printer) {
        this.printer = printer;
    }

    public String getString(){
        return scanner.nextLine();
    }

    public void close(){
        scanner.close();
    }


}
