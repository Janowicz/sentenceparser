package io.file;

import exception.CreateFileException;
import exception.DataExportException;
import exception.DataImportException;
import io.ConsolePrinter;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.Collectors;

public class CsvFileManager implements FileManager {

    private final static String CSVFILE = "src/main/resources/parsedSentence.csv";
    private ConsolePrinter consolePrinter;
    int wordsCounter = 0;

    public CsvFileManager(ConsolePrinter consolePrinter) {
        this.consolePrinter = consolePrinter;
    }

    @Override
    public void parse(String text) {
        System.out.println("Changes");
        StringBuilder sb = new StringBuilder();
        String[] split = text.split("\\.");
        wordsCounter = 0;

        for (int i = 0; i < split.length; i++) {
            if (split[i].equals("") || split[i].equals("\n"))
                continue;
            sb.append(splitWords(split[i], (i + 1)));
        }
        addCsvHeader(sb);
        exportData(sb.toString());
    }

    private void addCsvHeader(StringBuilder sb) {
        for (int i = wordsCounter; i > 0; i--) {
            sb.insert(0, (", Word " + (i)));
        }
    }

    @Override
    public void printData() {
        createFile();
        try (BufferedReader bf = new BufferedReader(new FileReader(CSVFILE))){
            bf.lines()
                    .forEach(consolePrinter::printLine);
        } catch (IOException e){
            throw new DataImportException("Error during read file!");
        }
    }

    @Override
    public void exportData(String data) {
        createFile();
        writeToFile(data);
    }

    private void createFile() {
        File file = new File(CSVFILE);
        try {
            if (!file.exists())
                file.createNewFile();
        } catch (IOException e){
            throw new CreateFileException("Error during creating file!");
        }
    }

    private void writeToFile(String content) {
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(CSVFILE))){
            bw.write(content);
        } catch (IOException e){
            throw new DataExportException("Error during write to file!");
        }
    }

    private String splitWords(String sentence, int sentenceNumber) {
        StringBuilder sb = new StringBuilder();
        String[] splitSentence = sentence.split("\\s|,");

        ArrayList<String> collect = Arrays.stream(splitSentence)
                .filter(x -> x.length() != 0)
                .map(String::trim)
                .sorted(String::compareToIgnoreCase)
                .collect(Collectors.toCollection(ArrayList::new));

        sb.append("\nSentence ")
            .append(sentenceNumber);
        getMaxWords(collect);
        addComaBetweenWords(sb, collect);
        return sb.toString();
    }

    private void getMaxWords(ArrayList<String> collect) {
        if (collect.size() > wordsCounter)
            wordsCounter = collect.size();
    }

    private void addComaBetweenWords(StringBuilder sb, ArrayList<String> collect) {
        Iterator<String> iterator = collect.iterator();
        while (iterator.hasNext()) {
            sb.append(", ")
                    .append(iterator.next());
        }
    }
}
