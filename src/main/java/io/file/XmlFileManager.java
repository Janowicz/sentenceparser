package io.file;

import exception.CreateFileException;
import exception.DataExportException;
import exception.DataImportException;
import io.ConsolePrinter;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.Collectors;

public class XmlFileManager implements FileManager {

    private final static String XMLFILE = "src/main/resources/parsedSentence.xml";
    private ConsolePrinter consolePrinter;

    public XmlFileManager(ConsolePrinter consolePrinter) {
        this.consolePrinter = consolePrinter;
    }

    @Override
    public void parse(String text) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?> ")
                .append("\n<text>");
        String[] split = text.split("\\.");
        for (int i = 0; i < split.length; i++) {
            if (split[i].equals("") || split[i].equals("\n"))
                continue;
            sb.append(splitWords(split[i]));
        }
        sb.append("\n</text>");
        exportData(sb.toString());
    }

    @Override
    public void printData() {
        createFile();
        try (BufferedReader bf = new BufferedReader(new FileReader(XMLFILE))){
            bf.lines()
                    .forEach(consolePrinter::printLine);
        } catch (IOException e){
            throw new DataImportException("Error during read file!");
        }
    }

    @Override
    public void exportData(String data) {
        createFile();
        writeToFile(data);
    }


    private void createFile() {
        File file = new File(XMLFILE);
        try {
            if (!file.exists())
                file.createNewFile();
        } catch (IOException e){
            throw new CreateFileException("Error during creating file!");
        }
    }

    private void writeToFile(String content) {
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(XMLFILE))){
            bw.write(content);
        } catch (IOException e){
            throw new DataExportException("Error during write to file!");
        }
    }

    private String splitWords(String sentence) {
        StringBuilder sb = new StringBuilder();
        String[] splitSentence = sentence.split("\\s|,");
        ArrayList<String> collect = Arrays.stream(splitSentence)
                .filter(x -> x.length() != 0)
                .sorted(String::compareToIgnoreCase)
                .collect(Collectors.toCollection(ArrayList::new));
        sb.append("\n\t<sentence>");
        Iterator<String> iterator = collect.iterator();
        while (iterator.hasNext()) {
            sb.append("\n\t\t<word>")
                    .append(iterator.next().trim())
                    .append("</word>");
        }
        sb.append("\n\t</sentence>");
        return sb.toString();
    }
}
