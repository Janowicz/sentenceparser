package io.file;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileManager {

    void exportData(String data) throws IOException;
    void printData() throws IOException;
    void parse(String text) throws IOException;

}
