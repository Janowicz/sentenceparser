package parserApp;

import exception.NoSuchOptionException;
import io.ConsolePrinter;
import io.DataReader;
import io.file.CsvFileManager;
import io.file.XmlFileManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;

public class Controller {

    private ConsolePrinter consolePrinter = new ConsolePrinter();
    private DataReader dataReader = new DataReader(consolePrinter);
    private CsvFileManager csvFileManager = new CsvFileManager(consolePrinter);
    private XmlFileManager xmlFileManager = new XmlFileManager(consolePrinter);

    void controlLoop() throws IOException {
        Option option;
        do {
            printOptions();
            option = getOption();
            switch (option){
                case READ_CSV:
                    readCsv();
                    break;
                case READ_XML:
                    readXml();
                    break;
                case PARSE_SENTENCE:
                    parseSentence();
                    break;
                case EXIT:
                    consolePrinter.printLine("Bye!");
                    System.exit(0);
                    break;
                default:
                    consolePrinter.printLine("There is not an option, try again.");
            }
        } while (option != Option.EXIT);
    }

    private void parseSentence() throws IOException {
        consolePrinter.printLine("Enter the sentences!");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringBuilder sb = new StringBuilder();
        String lines;
        while (!(lines = br.readLine()).equals("")){
            sb.append(lines.trim())
                .append("\n");
        }
        String string = sb.toString();
        xmlFileManager.parse(string);
        csvFileManager.parse(string);
    }

    private void readXml() {
        xmlFileManager.printData();
    }

    private void readCsv() {
        csvFileManager.printData();
    }

    private Option getOption() {
        boolean optionOk = false;
        Option option = null;
        while (!optionOk){
            try {
                option = Option.optionFromInt(Integer.parseInt(dataReader.getString()));
                optionOk = true;
            } catch (NoSuchOptionException e) {
                consolePrinter.printLine("Wrong option, try again");
            } catch (InputMismatchException e){
                consolePrinter.printLine("Enter number!");
            } catch (NumberFormatException e){
                System.err.println("Wrong format number");
            }
        }
        return option;
    }

    private void printOptions(){
        consolePrinter.printLine("\n---Enter option:---");
        for (Option value : Option.values()) {
            consolePrinter.printLine(value.toString());
        }
    }

    private enum Option{
        EXIT(0, "Exit"),
        READ_CSV(1, "Read CSV file"),
        READ_XML(2, "Read XML file"),
        PARSE_SENTENCE(3, "Parse sentence");

        private int value;
        private String description;

        Option(int value, String description) {
            this.value = value;
            this.description = description;
        }

        @Override
        public String toString() {
            return value + " - " + description;
        }

        static Option optionFromInt(int option) throws NoSuchOptionException {
            try {
                return Option.values()[option];
            } catch (ArrayIndexOutOfBoundsException e){
                throw new NoSuchOptionException("No options with id" + option);
            }
        }
    }
}
