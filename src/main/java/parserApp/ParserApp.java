package parserApp;

import java.io.IOException;

public class ParserApp {

    public static void main(String[] args) throws IOException {
        System.out.println("Parser to CVS and XML");
        Controller controler = new Controller();
        controler.controlLoop();
    }
}
